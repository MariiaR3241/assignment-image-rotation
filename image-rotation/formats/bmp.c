#include "bmp.h"

uint32_t get_padding(const uint64_t width) {
  size_t temp_width = width * sizeof(struct pixel);
  if (temp_width % PADDING_SIZE == 0) return 0;
  return PADDING_SIZE - (temp_width % PADDING_SIZE);
}

struct bmp_header create_bmp_header(struct image const img) {
  struct bmp_header header = {
      .bfType = BMP_TYPE,
      .bfileSize = sizeof(struct bmp_header) + (sizeof(struct pixel) * img.width + img.width % 4) * img.height,
      .bfReserved = 0,
      .bOffBits = sizeof(struct bmp_header),
      .biSize = 40,
      .biWidth = img.width,
      .biHeight = img.height,
      .biPlanes = 1,
      .biBitCount = COLOR_DEPTH,
      .biCompression = 0,
      .biSizeImage = img.width * img.height * sizeof(struct pixel),
      .biXPelsPerMeter = 0,
      .biYPelsPerMeter = 0,
      .biClrUsed = 0,
      .biClrImportant = 0,
  };

  return header;
}

enum read_status from_bmp(FILE* in, struct image* img) {
  struct bmp_header header = {0};
  if (fread(&header, sizeof(struct bmp_header), 1, in) < 1) {
    if (feof(in)) return READ_INVALID_BITS;
    return READ_INVALID_HEADER;
  }

  if (header.bfType != BMP_TYPE) return READ_INVALID_SIGNATURE;
  if (header.biBitCount != COLOR_DEPTH) return READ_UNSUPPORTED_DEPTH;

  if (fseek(in, header.bOffBits, SEEK_SET) != 0) return READ_INVALID_BITS;

  const uint32_t padding = get_padding(header.biWidth);
  const uint32_t pixel_size = sizeof(struct pixel);
  struct pixel* data = malloc(header.biWidth * header.biHeight * pixel_size);

  for (uint64_t y = 0; y < header.biHeight; ++y) {
    if (fread(data + y * header.biWidth, pixel_size, header.biWidth, in) < pixel_size) return READ_INVALID_BITS;
    if (fseek(in, padding, SEEK_CUR) != 0) return READ_INVALID_BITS;
  }

  if (fseek(in, 0, SEEK_SET) != 0) return READ_INVALID_BITS;

  img->width = header.biWidth;
  img->height = header.biHeight;
  img->data = data;

  return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
  const struct bmp_header header = create_bmp_header(*img);
  if (fwrite(&header, sizeof(struct bmp_header), 1, out) < 1) return WRITE_ERROR;

  const uint32_t padding = get_padding(img->width);
  const uint32_t pixel_size = sizeof(struct pixel);
  const char padding_bytes[3] = {0};

  for (uint64_t y = 0; y < img->height; ++y) {
    if (fwrite(img->data + y * img->width, pixel_size, img->width, out) != img->width) return WRITE_INVALID_BITS;
    if (fflush(out) != 0) return WRITE_ERROR;
    if (fwrite(padding_bytes, padding, 1, out) != 1 && padding != 0) return WRITE_INVALID_BITS;
    if (fflush(out) != 0) return WRITE_ERROR;
  }

  if (fseek(out, 0, SEEK_SET) != 0) return WRITE_INVALID_BITS;
  return WRITE_OK;
}
