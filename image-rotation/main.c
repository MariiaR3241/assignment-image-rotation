#include "utils/file.h"
#include "utils/message.h"
#include "formats/bmp.h"
#include "processing/rotation.h"

void usage() {
  print("Usage: image_rotation <path/to/bmp/file>");
}

int main(int argc, char** argv) {
  if (argc != 2 || argv[1] == NULL) usage();
  if (argc < 2) error("Not enough arguments");
  if (argc > 2) error("Too many arguments");

  struct image img = {0};

  FILE* image_file = NULL;
  enum open_status os = open_file(&image_file, argv[1]);
  if (os != OPEN_OK)
    error("Error opening bmp file with path: %s, code: %d, message: %s",
          argv[1], os, os_to_message(os));

  enum read_status rs = from_bmp(image_file, &img);
  if (rs != READ_OK)
    error("Error reading bmp file, code: %d, message: %s",
          rs, rs_to_message(rs));

  struct image new_image = rotate(img);

  enum write_status ws = to_bmp(image_file, &new_image);
  if (ws != WRITE_OK)
    error("Error writing to bmp file, code: %d, message: %s",
          ws, ws_to_message(ws));

  enum close_status cs = close_file(&image_file);
  if (cs != CLOSE_OK)
    error("Error closing image file, code: %d, message: %s",
          cs, cs_to_message(cs));

  print("Image rotated successfully");
  return 0;
}
