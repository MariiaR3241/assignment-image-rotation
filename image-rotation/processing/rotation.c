#include "rotation.h"

struct image rotate(struct image const source) {
  struct image new_image = {
      .width = source.height,
      .height = source.width,
      .data = malloc(source.width * source.height * sizeof(struct pixel))
  };

  for (uint64_t y = 0; y < source.height; ++y) {
    for (uint64_t x = 0; x < source.width; ++x) {
      *(new_image.data + x * new_image.width + new_image.width - y - 1) = *(source.data + y * source.width + x);
    }
  }

  return new_image;
}
