#ifndef IMAGE_ROTATION_PROCESSING_ROTATION_H_
#define IMAGE_ROTATION_PROCESSING_ROTATION_H_

#include <stdlib.h>
#include "../image.h"

struct image rotate(struct image const source);

#endif //IMAGE_ROTATION_PROCESSING_ROTATION_H_
