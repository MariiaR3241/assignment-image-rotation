#include "file.h"

enum open_status open_file(FILE** file, const char* path) {
  *file = fopen(path, "rb+");
  if (*file == NULL) return OPEN_ERROR;
  return OPEN_OK;
}

enum close_status close_file(FILE** file) {
  if (fclose(*file) != 0) return CLOSE_ERROR;
  return CLOSE_OK;
}
