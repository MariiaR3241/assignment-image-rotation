#include "message.h"

void print(const char* msg, ...) {
  va_list args;
  va_start(args, msg);
  vfprintf(stdout, msg, args);
  fprintf(stdout, "\n");
  va_end(args);
}

void error(const char* msg, ...) {
  va_list args;
  va_start(args, msg);
  vfprintf(stderr, msg, args);
  fprintf(stderr, "\n");
  va_end(args);
  exit(1);
}

const char* os_messages[] = {
    "open file successfully",
    "open file error, check if file exists and its access rights"
};
const char* unresolved_os_message = "unresolved open error";
const char* os_to_message(const enum open_status os) {
  if (os < 0 || os > 2) return unresolved_os_message;
  return os_messages[os];
}

const char* rs_messages[] = {
    "read file successfully",
    "invalid bits",
    "invalid header",
    "invalid signature",
    "unsupported color depth (only 24 supported)"
};
const char* unresolved_rs_message = "unresolved read error";
const char* rs_to_message(const enum read_status rs) {
  if (rs < 0 || rs > 5) return unresolved_rs_message;
  return rs_messages[rs];
}

const char* ws_messages[] = {
    "write file successfully",
    "error writing to file",
    "invalid bits"
};
const char* unresolved_ws_message = "unresolved write error";
const char* ws_to_message(const enum write_status ws) {
  if (ws < 0 || ws > 3) return unresolved_ws_message;
  return ws_messages[ws];
}

const char* cs_messages[] = {
    "close file successfully",
    "close file error",
};
const char* unresolved_cs_message = "unresolved close error";
const char* cs_to_message(const enum close_status cs) {
  if (cs < 0 || cs > 2) return unresolved_cs_message;
  return cs_messages[cs];
}
