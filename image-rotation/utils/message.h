#ifndef IMAGE_ROTATION_UTILS_MESSAGE_H_
#define IMAGE_ROTATION_UTILS_MESSAGE_H_

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include "file.h"
#include "../formats/bmp.h"

void print(const char* msg, ...);
void error(const char* msg, ...);

const char* os_to_message(const enum open_status os);
const char* rs_to_message(const enum read_status rs);
const char* ws_to_message(const enum write_status ws);
const char* cs_to_message(const enum close_status cs);

#endif //IMAGE_ROTATION_UTILS_MESSAGE_H_
